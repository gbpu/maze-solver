# Maze solver 

Python code that solves mazes similar to [www.mazegenerator.net](www.mazegenerator.net)


## How to run
1.  Download a maze image from [www.mazegenerator.net](www.mazegenerator.net) or use from Maze_example folder

![20 by 20 unsolveld maze](./20_by_20_unsolved_maze.png)

2.  Run `maze_solver.py` 

- The programm will ask you to enter with the path to the maze image and to enter with the size of the mesh. 
    To run one of the examples, type `Maze_examples/20 by 20 orthogonal maze.png` and then `20 20`.

3. An image of the solution will be saved in the current folder. 
Following the example: "SOLUTION20x20.png"

![20 by 20 solveld maze](./SOLUTION20x20.png)



